package com.devcamp.api.j56abasicrespai.coltrollers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@RequestMapping("/api")
public class SplitController {
    @GetMapping("/split")
    public ArrayList<String> splitString(@RequestParam(value="inputString", defaultValue = "mot hai ba bon") String input) {
        ArrayList<String> lstString = new ArrayList<>();
        String[] lstInput = input.split(" ");
        
        lstString = new ArrayList<>(Arrays.asList(lstInput));
        
        return lstString;
    }    
}
