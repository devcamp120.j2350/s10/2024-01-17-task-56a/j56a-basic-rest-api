package com.devcamp.api.j56abasicrespai.coltrollers;

import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



@RestController
@RequestMapping("/api")
public class RainbowController {
    @GetMapping("/rainbow")
    public ArrayList<String> getRainbowList() {
        ArrayList<String> listRainbows = new ArrayList<>();
        String[] rainbows = {"red", "orange", "yellow", "green", "blue", "indigo", "violet"};

        for (String rainbow : rainbows) {
            listRainbows.add(rainbow);
        }

        //listRainbows = new ArrayList<>(Arrays.asList(rainbows));
        return listRainbows;
    }
    
}
