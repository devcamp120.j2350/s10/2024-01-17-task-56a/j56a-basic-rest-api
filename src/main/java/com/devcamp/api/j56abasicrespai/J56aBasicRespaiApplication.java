package com.devcamp.api.j56abasicrespai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J56aBasicRespaiApplication {

	public static void main(String[] args) {
		SpringApplication.run(J56aBasicRespaiApplication.class, args);
	}

}
